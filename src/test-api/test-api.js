import { LitElement, html } from 'lit-element';

class TestApi extends LitElement{

    static get properties(){
        return{
            movies: {type: Array}
        }
    }

    constructor(){
        super();

        this.movies = [];
        this.getMovieData();
    }

    render(){
        return html`
            ${this.movies.map(
                movie => html`<div>La pelicula ${movie.title}, fue dirigida por ${movie.director}</div><br />`
            )}        

        `;
    }

    getMovieData() {
        console.log("getMovieData");
        console.log("Obteniendo datos de las películas");
        
        let xhr = new XMLHttpRequest(); //let crea variable sólo en el bloque (funcion, bucle, ...) donde se declara

        xhr.onload = () =>{
            if(xhr.status === 200){
                console.log("Petición completada correstamente");

                let APIResponse = JSON.parse(xhr.responseText); 
                
                //console.log(APIResponse);
                this.movies = APIResponse.results;
                
            }
        };

        xhr.open("GET", "https://swapi.dev/api/films/");
        xhr.send();
        console.log("Fin de getMovieData");

    }

}

customElements.define('test-api', TestApi)