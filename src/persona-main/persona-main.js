import { LitElement, html} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'

class PersonaMain extends LitElement{

    static get properties(){
        return{
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }
  
    constructor(){
        super();

        this.people = [
            {
                name: "Ellen Ripley",
                yearsInCompany: 10,
                profile: "Lorem ipsum dolor sit ame",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Ellen Ripley"
                }
            },{
                name: "Bruce Banner",
                yearsInCompany: 2,
                profile: "Lorem ipsum.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Bruce Banner"
                }
            },{
                name: "Eowyn",
                yearsInCompany: 5,
                profile: "Lorem ipsum dolor sit ame, consectetur.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Eowyn"
                }
            },{
                name: "Turanga Leels",
                yearsInCompany: 5,
                profile: "Lorem ipsum dolor.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Turanga Leels"
                }
            },{
                name: "Tyrion",
                yearsInCompany: 5,
                profile: "Lorem.",
                photo: {
                    src: "./img/persona.jpg",
                    alt: "Tyrion"
                }
            }   
        ];
        this.showPersonForm = false;
    }
    
    render(){
        return html`
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">            
                ${this.people.map(
                    person => html`
                    <persona-ficha-listado
                        fname="${person.name}" 
                        yearsInCompany="${person.yearsInCompany}"
                        profile="${person.profile}"
                        .photo="${person.photo}"
                        @delete-person="${this.deletePerson}"
                        @info-person="${this.infoPerson}"
                    >
                    </persona-ficha-listado>`

                )} 
                </div>
            </div>
            <div class="row">
                <persona-form id="personForm" class="d-none border rounded border-primary"
	                @persona-form-close="${this.personFormClose}"
                	@persona-form-store="${this.personFormStore}" >
                </persona-form>     
               
            </div>                 
        `;
    }

    updated(changedProperties) { 
        console.log("updated");
        //console.log(changedProperties)	

        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");
            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la pripiedad people en persona-main");

            this.dispatchEvent(new CustomEvent("updated-people", {
                    "detail": {
                        people: this.people
                    }
                }
            ));
        
        }
    }

    showPersonFormData() {
        console.log("showPersonFormData");
        console.log("Mostrando formulario de persona");
        
        this.shadowRoot.getElementById("personForm").classList.remove("d-none");	  
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");	
    } 	 

    showPersonList() {
        console.log("showPersonList");
        console.log("Mostrando listado de personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");	
    }  
    
    personFormClose() {
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de la persona");
        this.showPersonForm = false;
    }
    
    personFormStore(e) {
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");	
        
        console.log("La propiedad name vale " + e.detail.person.name);
        console.log("La propiedad profile vale " + e.detail.person.profile);
        console.log("La propiedad yearsInCompany vale " + e.detail.person.yearsInCompany);	
        console.log("La propiedad editingPerson vale " + e.detail.editingPerson);	
        
        if(e.detail.editingPerson === true){
            console.log("Se va a ctualizar la persona de nombre: " + e.detail.person.name)
            
            this.people = this.people.map(
               //operador ternario es como un if <condicion> ? <ejecuta si true> : <ejecuta si false>
                person => person.name === e.detail.person.name ? person = e.detail.person : person
           ); 

            /*let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            );

            if (indexOfPerson >= 0) {
                console.log("Persona encontrada");
                this.people[indexOfPerson] = e.detail.person;
            }*/

        } else{
            console.log("Se va a almacenar una persona nueva");
            //this.people.push(e.detail.person);
            //los ... (spread syntax) hace referencia al array entero, como que lo descompone en todos los eltos
            // en este caso añadimos el último elemento
            this.people = [...this.people, e.detail.person];
        }
       
        console.log("Persona almacenada");	
        
        this.showPersonForm = false;
    }

    deletePerson(e){
        console.log("deletePerson en persona-main")
        console.log("Se va a borrar la persona de nombre " + e.detail.name);
        
        this.people= this.people.filter(
            person => person.name != e.detail.name

        );
    }

    infoPerson(e) {
        console.log("infoPerson");
        console.log("Se ha pedido más información de la persona de nombre: " + e.detail.name);

        // Recuperamos la persona seleccionada 
        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );
        console.log(chosenPerson);
        
        // Para que al pulsar boton atras no se cargue el form con los datos cambiados
        let personToShow = {};
        personToShow.name = chosenPerson[0].name;
        personToShow.profile = chosenPerson[0].profile;
        personToShow.yearsInCompany = chosenPerson[0].yearsInCompany;

        //Preparamos el formulario a mostrar
                
        this.shadowRoot.getElementById("personForm").person = personToShow;
        // Decimos que vamos a editar no a dar de alta
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        //Inyectamos los valores en persona-form.js del modo:  .value="${this.person.name}
        
        //Mostramos el formulario    
        this.showPersonForm = true;
        

    }

}

customElements.define('persona-main', PersonaMain)