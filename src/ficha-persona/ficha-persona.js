import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement{
    
    static get properties(){
        return{
                name:{type: String},
                yearsInCompany: {type: Number},
                personInfo: {type: String},
                photo: {type: Object}
        };
    }

    constructor(){
        super();

        this.name = "Prueba Nombre";
        this.yearsInCompany = 20
        this.updatePersonInfo();
    }

    render(){
        return html`
            <div>Ficha Persona</div><br>
            <div>
                <label>Nombre Completo</label>
                <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br/><br/>
                <label>Años en la Empresa</label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br/><br/>
                <input type="text" value= "${this.personInfo}" disabled> </input>
                <br/>
            </div>
        `;
    }

    updated(changeProperties){
        //se llama cuando se ha pintado y actualizado el componente

        console.log("Entra en change");
        changeProperties.forEach((oldValue, propName) => {
            //console.log("Propiedad: " + propName + " cambia valor, anterior era: " + oldValue);
        });

        if (changeProperties.has("name")){
            console.log("Propiedad name cambia valor anterior era: " + changeProperties.get("name") + " nuevo es: " + this.name);
        }
        if (changeProperties.has("yearsInCompany")){
            console.log("Propiedad yearsInCompany cambia valor anterior era: " + changeProperties.get("yearsInCompany") + " nuevo es: " + this.yearsInCompany);
            this.updatePersonInfo();
        }
                
    }

    updateName(e){
        console.log("Ha cambiado el name");
        this.name = e.target.value
    }

    updateYearsInCompany(e){
        console.log("Entra en updateyearsInCompany")
        this.yearsInCompany = e.target.value;
    }
    
    updatePersonInfo(){
        console.log("Entra en UpdtePersoninfo");
        
        if (this.yearsInCompany >= 7 ){
            this.personInfo = "lead";
        } else if (this.yearsInCompany >= 5) {
            this.personInfo = "senior";
        } else if (this.yearsInCompany >= 3) {
            this.personInfo = "team";
        } else{
            this.personInfo = "junior";
        }
        
    }
   

}

customElements.define('ficha-persona', FichaPersona)