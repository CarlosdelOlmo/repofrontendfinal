import { LitElement, html } from 'lit-element';

class HolaMundo extends LitElement{
    render(){
        return html`
            <div>Hello world!</div>
        `;
    }
}

customElements.define('hola-mundo', HolaMundo)